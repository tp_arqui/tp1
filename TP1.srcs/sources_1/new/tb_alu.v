`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/25/2020 08:52:54 PM
// Design Name: 
// Module Name: tb_alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_alu();
    
    // LOCAL_PARAMETERS
    localparam NB_DATA = 8;
    localparam NB_CODE = 6;
    localparam NB_DATA_OUT = 8;
    
    localparam  ADD = 6'b100000;
    localparam  SUB = 6'b100010;
    localparam  AND = 6'b100100;
    localparam  OR  = 6'b100101;
    localparam  XOR = 6'b100110;
    localparam  SRA = 6'b000011;
    localparam  SRL = 6'b000010;
    localparam  NOR = 6'b100111;
    localparam  NOP = 6'b1;
    
    // TB_SIGNALS
    reg                                 clk;
    reg                                 test_start;
    reg                                 bool;
    reg         [NB_DATA-1 : 0]         data1;
    reg         [NB_DATA-1 : 0]         data2;
    reg         [NB_CODE-1 : 0]         op;
    wire        [NB_DATA_OUT-1 : 0]     res;
    
    initial begin
        clk = 1'b0;
        bool = 1'b0;
        data1 = {NB_DATA{1'b0}};
        data2 = {NB_DATA{1'b0}};
        op = {NB_CODE{1'b0}};
        test_start = 1'b0;
        
        #100
        op = ADD;
        test_start = 1'b1;
        
        #100
        op = SUB;
        
        #100
        op = AND;
        
        #100
        op = OR;
        
        #100
        op = XOR;
        
        #100
        op = SRA;
        
        #100
        op = SRL;
        
        #100
        op = NOR;
        
        #100
        op = NOP;
        
        #1000
        
        $display("############# Test OK ############");
        $finish();
    end
    
    // MODULE_INSTANCE
    alu
    #(
        .NB_DATA    (NB_DATA),
        .NB_DATA_OUT(NB_DATA_OUT)
    )
    u_alu
    (
        .i_a        (data1),
        .i_b        (data2),
        .i_op       (op),
        .o_r        (res)
    );
    
    // CLOCK_GENERATION
    always @(posedge clk) begin
        data1 <= $urandom();
        data2 <= $urandom(); 
    end

    // CLOCK_GENERATION
  	always #10 clk = ~clk;
    
    // CHECK_MODULE_OUTPUT
    always @(posedge clk) begin
        if(test_start) begin
             case(op)
                // ADD
                ADD:
                bool = (res != (data1 + data2));
                // SUB
                SUB:
                bool = (res != (data1 - data2));
                // AND
                AND:
                bool = (res != (data1 & data2));
                // OR
                OR:
                bool = (res != (data1 | data2));
                // XOR
                XOR:
                bool = (res != (data1 ^ data2));
                // SRA
                SRA:
                bool = (res != (data1 >>> data2));
                // SRL
                SRL:
                bool = (res != (data1 >> data2));
                // NOR
                NOR:
                bool = (res != (~(data1 | data2)));  
            endcase
            if(bool) begin
                $error("Error en la operacion [%c]", op);
                $display("############# Test FALLO ############");
                $finish();
            end
        end           
    end
endmodule
