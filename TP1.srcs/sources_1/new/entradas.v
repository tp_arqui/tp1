`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.09.2020 17:34:05
// Design Name: 
// Module Name: tb_alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module entradas
#( 
	parameter BUS_SIZE = 8
	)
(
	input wire [BUS_SIZE-1:0] i_switches,
	input wire boton1, boton2, boton3,
	output wire [BUS_SIZE-1:0] leds,
	input wire clk
    );
	
	reg [BUS_SIZE-1:0] i_datoA, i_datoB, i_Op;

	always @(posedge clk) 
	begin
		if (boton1 && ~boton2 && ~boton3) 
			begin
				i_datoA <= i_switches;		
			end
		else if (boton2 && ~boton1 && ~boton3)
			begin
				i_datoB <= i_switches;
			end
		else if (boton3 && ~boton1 && ~boton2) 
			begin
				i_Op <= i_switches;
			end
	end

	alu
	#(
		.BUS_SIZE (BUS_SIZE)
		)
	Alu
	(	
		.A(i_datoA), .B(i_datoB), .Op(i_Op),
		.salida(leds)

		);


endmodule
