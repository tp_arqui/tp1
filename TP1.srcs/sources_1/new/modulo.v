`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.09.2020 22:31:11
// Design Name: 
// Module Name: modulo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module modulo 
#(
    parameter NB_BUTTONS = 4,
    parameter NB_DATA = 8,
    parameter NB_CODE = 6
)

(   
    // INPUTS
    input wire                         i_clk,
    input wire      [NB_DATA-1:0]      i_switch,
    input wire      [NB_BUTTONS-1:0]   i_buttons,
    
    // OUTPUTS
    output wire     [NB_DATA-1:0]  o_leds
);
        
    // INTERNAL 
    reg             [NB_DATA-1:0]  r_data1;
    reg             [NB_DATA-1:0]  r_data2;
    reg             [NB_CODE-1:0]  r_op;
    reg             [NB_DATA-1:0]  r_res;
    wire            [NB_DATA-1:0]  r_aux;
    
    assign o_leds = r_res;
    
    always @(posedge i_clk) begin
        case(i_buttons)
            4'b0001: r_data1 <= i_switch;
            4'b0010: r_data2 <= i_switch;
            4'b0100: r_op    <= i_switch;
            4'b1000: r_res   <= r_aux;
        endcase
    end

    // MODULE_INSTANCE
    alu
    #(
        .NB_DATA    (NB_DATA),
        .NB_DATA_OUT(NB_DATA)
    )
    u_alu
    (
        .i_a        (r_data1),
        .i_b        (r_data2),
        .i_op       (r_op),
        .o_r        (r_aux)
    );			
endmodule
