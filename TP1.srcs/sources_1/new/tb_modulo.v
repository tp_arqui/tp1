`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/26/2020 10:30:21 PM
// Design Name: 
// Module Name: tb_modulo
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_modulo();

    // LOCAL_PARAMETERS
    localparam NB_DATA = 8;
    localparam NB_BUTTONS = 4;
    
    localparam  ADD = 6'b100000;
    localparam  SUB = 6'b100010;
    localparam  AND = 6'b100100;
    localparam  OR  = 6'b100101;
    localparam  XOR = 6'b100110;
    localparam  SRA = 6'b000011;
    localparam  SRL = 6'b000010;
    localparam  NOR = 6'b100111;
    localparam  NOP = 6'b1;
    
    localparam  b_data1 = 4'b0001;
    localparam  b_data2 = 4'b0010;
    localparam  b_op    = 4'b0100;
    localparam  b_res   = 4'b1000;
    localparam  b_up    = 4'b0000;

	// TB_SIGNALS
    reg                                 clk;
    reg                                 test_start;
    reg         [NB_DATA-1 : 0]         switch;
    reg         [NB_BUTTONS-1 : 0]      buttons;
    wire        [NB_DATA-1 : 0]     	leds;
    
    initial begin
    	$dumpfile("dump.vcd");
    	$dumpvars;
        clk 		= 1'b0;
        switch 		= {NB_DATA{1'b0}};
        buttons		= {NB_DATA{1'b0}};
        test_start  = 1'b0;
        
        #5
        test_start = 1'b1;
        
        #50
        switch = 50;//$urandom();
        #50
        buttons = b_data1;
        #50
        buttons = b_up;
        #50
        switch = 25;//$urandom();
        #50
        buttons = b_data2;
        #50
        buttons = b_up;
        #50
        switch = ADD;
        #50
        buttons = b_op;
        #50
        buttons = b_up;
        #100
        buttons = b_res;
        #50
        buttons = b_up;
               
        #1500
        
        $display("############# Test OK ############");
        $finish();
    end
    
    // MODULE_INSTANCE
    modulo
    #(
        .NB_BUTTONS (NB_BUTTONS),
        .NB_DATA    (NB_DATA)
    )
    u_modulo
    (
    	.i_clk		(clk),
        .i_switch   (switch),
        .i_buttons  (buttons),
        .o_leds     (leds)
    );
    
    // CLOCK_GENERATION
  	always #10 clk = ~clk;

endmodule
